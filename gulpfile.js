'use strict'

var gulp = require('gulp');
var csso = require('gulp-csso');
var imagemin = require('gulp-imagemin');
var posthtml = require('gulp-posthtml');
var sass = require('gulp-sass');
var pug = require('gulp-pug');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create;

gulp.task('default', function(){
    console.log("hallo")
});

gulp.task('sass', function(){
    return gulp.src('./src/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./src/css'));
});
gulp.task('sass:watch', function(){
    gulp.watch('./src/**/*.scss', ['sass']);
});

gulp.task('serve', ['sass'], function() {
        browserSync.init({
            server: "./src/*.scss"
});

gulp.task('serve', ['html'], function() {  
         browserSync.init({
                    server: "./src/*.html"
});           
gulp.watch("./src/*.scss", ['sass']);
    gulp.watch("./src/*.html").on('change', browserSync.reload);
});
gulp.task('sass', function() {
    return gulp.src("./src/**/*.scss")
        .pipe(sass())
        .pipe(gulp.dest("./src/.css"))
        .pipe(browserSync.stream());
});
gulp.task('html', function() {
    return gulp.src("./src/html/*.html")
        .pipe(html())
        .pipe(gulp.dest("./src/html"))
        .pipe(browserSync.stream());
});    
gulp.task('browserSync', ['serve']);
    gulp.task('serve', function () {
     browserSync.init({
        server: {
            baseDir: "./src"
        }
    });
 gulp.watch("*.html").on("change", reload);
});

gulp.task('style', function(){
    return gulp.src("./src/scss/**/*.scss")
        .pipe(sass())
        .pipe(postcss([
            autoprefixer()
        ]))
        .pipe(gulp.dest("./src/css"))
        .pipe(minifyCss())
        .pipe(rename("style.min.css"))
        .pipe(gulp.dest("./src/css"))
        .pipe(browserSync.stream())
});

gulp.task('autoprefixer', function(){
    return gulp.src('./src/**/*.css')
    .pipe(autoprefixer('last 2 version'))
    .pipe(concat(css))
    .pipe(gulp.dest('dist'))
});

gulp.task('csso', function(){
    return gulp.src('./src/**/*.css')
    .pipe(csso())
    .pipe(gulp.dest('./src/css'))
});

gulp.task('imagemin', () =>
   gulp.src('./src/img/*.jpeg')
   .pipe(imagemin())
   .pipe(gulp.dest('dist/img'))
)}
);